<div align="center">

<h1>
<img width="25" src="https://gitlab.com/hmajid2301/nixicle/-/raw/main/images/logo.png"></img> dotfiles
</h1>

<img src="https://img.shields.io/badge/Distro-NixOS-b7bdf8?&labelColor=363a4f&style=for-the-badge&logo=NixOS&logoColor=white" height="22">
<img src="https://img.shields.io/gitlab/stars/mbednarek360/dots?color=eed49f&labelColor=363a4f&style=for-the-badge&logo=stackblitz&logoColor=white" height="22">
<img src="https://img.shields.io/gitlab/last-commit/mbednarek360/dots?color=a6da95&labelColor=363a4f&style=for-the-badge&logo=gitlab&logoColor=white" height="22">
<br><br>

![](screenshots/2.png){width=175}
![](screenshots/1.png){width=175}
![](screenshots/3.png){width=175}
<br><br>
<img src="https://raw.githubusercontent.com/catppuccin/catppuccin/main/assets/palette/macchiato.png" width="400" />

</div>

---

### 📄 Structure

* `flake.nix` : The heart of it all
* `hosts/` : Nix host-specific configs    
* `lib/` : Various commonly used functions   
* `modules/`        
    * `system/` : Common configs for all systems
    * `gui/` : Configs for desktop/laptop use
    * `services/` : Configs for server use
    * `misc/` : Specific addons for various systems

---

### 🖥️ Installation

> :warning: **For personal reference only.** Many options must be changed, including the secrets repo, before usage.

<br>

```sh 
$ git clone https://gitlab.com/mbednarek360/dots ~/Dots
```
```sh 
$ sudo nixos-rebuild boot --flake ~/Dots
```
