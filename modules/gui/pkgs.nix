{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    hyprpaper
    hypridle
    hyprlock
    kitty
    capitaine-cursors
    dunst
    rofi-wayland
    hyprshot
    wl-clipboard
    libnotify
    brightnessctl
    alsa-utils
    pulsemixer
    cava
    libsecret
    piper
    pipes
    poweralertd
    easyeffects
    (callPackage ./pkgs/work2d { })
   
    typst
    typstyle
    tinymist

    keepassxc
    brave
    obsidian
    multiviewer-for-f1
  ];

  fonts.packages = with pkgs; [
    roboto
    source-sans-pro
    source-sans
    font-awesome
    nerd-fonts.victor-mono
    # (nerdfonts.override { fonts = [ "VictorMono" ]; })
  ];

  xdg.mime.defaultApplications = {
    "text/html" = "brave-browser.desktop";
    "x-scheme-handler/http" = "brave-browser.desktop";
    "x-scheme-handler/https" = "brave-browser.desktop";
    "x-scheme-handler/about" = "brave-browser.desktop";
    "x-scheme-handler/unknown" = "brave-browser.desktop";
    "image/jpeg" = "brave-browser.desktop";
    "image/png" = "brave-browser.desktop";
    "image/svg+xml" = "brave-browser.desktop";
    "image/webp" = "brave-browser.desktop";
    "video/mp4" = "brave-browser.desktop";
    "video/mpeg" = "brave-browser.desktop";
    "video/webm" = "brave-browser.desktop";
    "application/pdf" = "brave-browser.desktop";
  };

  environment.persistence."/perm".users.mrb.directories = [
    ".cache/keepassxc"
    ".config/keepassxc"
    ".config/MultiViewer for F1"
    ".config/obsidian"
    ".local/state/wireplumber"
    {
      directory = ".config/BraveSoftware";
      mode = "0700";
    }
  ];
}
