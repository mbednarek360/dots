{ pkgs, ... }:

{
  services.greetd = {
    enable = true;
    vt = 3;
    settings = rec {
      initial_session = {
        user = "mrb";
        command = "Hyprland";
      };
      default_session = initial_session;
    };
  };

  services.gnome.gnome-keyring.enable = true;
  security.pam.services.greetd.enableGnomeKeyring = true;
}
