use criterion::{criterion_group, criterion_main, Criterion};
use std::hint::black_box;

#[path = "../src/common/mod.rs"]
mod common;
#[path = "../src/overview/mod.rs"]
mod overview;
#[path = "../src/workspace/mod.rs"]
mod workspace;                                                                        
use crate::overview::draw_frame;

fn criterion_benchmark(c: &mut Criterion) {
    let mut terminal = ratatui::init();
    terminal.clear().unwrap();
    let gaps = common::get_gaps();
    c.bench_function("draw_frame", |b| {
        b.iter(|| draw_frame(black_box(&mut terminal), black_box(gaps)))
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
