use crate::common;
use serde_json;
use std::env;
use std::io::prelude::*;
use std::os::unix::net::UnixStream;

const MAX_DEN: u32 = 1000;

#[derive(Clone)]
pub struct Client {
    pub ws: (u64, u64),
    pub class: String,
    pub pos: (i64, i64),
    pub size: (u64, u64),
    pub float: bool,
}

fn get_sock_path() -> String {
    let xdg = env::var("XDG_RUNTIME_DIR").unwrap();
    let hypr = env::var("HYPRLAND_INSTANCE_SIGNATURE").unwrap();
    format!("{}/hypr/{}/.socket.sock", xdg, hypr)
}

fn send_cmd(cmd: String, ret_json: bool) -> Option<serde_json::Value> {
    let command = if ret_json {
        format!("[j]/{}", cmd)
    } else {
        cmd
    };

    let mut stream = UnixStream::connect(get_sock_path()).unwrap();
    stream.write_all(command.as_str().as_bytes()).unwrap();

    if ret_json {
        let mut response = String::new();
        stream.read_to_string(&mut response).unwrap();
        Some(serde_json::from_str(response.as_str()).unwrap())
    } else {
        None
    }
}

pub fn set_ws(id: u64, mv: bool, silent: bool) {
    let cmd = if silent {
        "movetoworkspacesilent"
    } else if mv {
        "movetoworkspace"
    } else {
        "workspace"
    };
    send_cmd(format!("dispatch {} {}", cmd, id), false);
}

pub fn change_anim(vert: bool) {
    let anim = if vert { "slidevert" } else { "slide" };
    send_cmd(
        format!("keyword animation workspaces, 1, 2, default, {}", anim),
        false,
    );
}

// x, y, class, pos, size
pub fn parse_cache(mon: u64) -> Vec<Client> {
    let mut pairs: Vec<((u64, u64), u64, String, (i64, i64), (u64, u64), bool)> =
        send_cmd("clients".to_string(), true)
            .unwrap()
            .as_array()
            .unwrap()
            .into_iter()
            .filter_map(|client| {
                let id = client["workspace"]["id"].as_i64().unwrap();
                if client["monitor"].as_u64().unwrap() == mon
                    && id >= 0
                    && client["class"].as_str().unwrap() != "work2d"
                {
                    Some((
                        common::parse_id(id as u64),
                        client["focusHistoryID"].as_u64().unwrap(),
                        client["class"].as_str().unwrap().to_string(),
                        (
                            client["at"].as_array().unwrap()[0].as_i64().unwrap(),
                            client["at"].as_array().unwrap()[1].as_i64().unwrap(),
                        ),
                        (
                            client["size"].as_array().unwrap()[0].as_u64().unwrap(),
                            client["size"].as_array().unwrap()[1].as_u64().unwrap(),
                        ),
                        client["floating"].as_bool().unwrap(),
                    ))
                } else {
                    None
                }
            })
            .collect();
    pairs.sort_unstable_by_key(|p| p.1);
    pairs
        .into_iter()
        .map(|p| Client {
            ws: p.0,
            class: p.2,
            pos: p.3,
            size: p.4,
            float: p.5,
        })
        .collect()
}

pub fn switch_mon(id: u64) {
    send_cmd(format!("dispatch focusmonitor {}", id), false);
}

fn get_mon_arr() -> Vec<serde_json::Value> {
    let js = send_cmd("monitors".to_string(), true).unwrap();
    js.as_array().unwrap().clone()
}

pub fn get_mon_ids() -> Vec<u64> {
    let arr = get_mon_arr();
    arr.iter()
        .map(|mon| mon["id"].as_u64().unwrap())
        .collect::<Vec<u64>>()
}

// ws, mon id, pos, res
pub fn get_current() -> (u64, u64, (u64, u64), (u64, u64), f64) {
    let arr = get_mon_arr();
    let mon = arr
        .iter()
        .enumerate()
        .filter_map(|(index, mon)| {
            if mon["focused"].as_bool().unwrap() {
                Some((index, mon["id"].as_u64().unwrap()))
            } else {
                None
            }
        })
        .collect::<Vec<(usize, u64)>>()[0];
    let stats = arr[mon.0].clone();
    let res = (
        stats["width"].as_u64().unwrap(),
        stats["height"].as_u64().unwrap(),
    );
    (
        stats["activeWorkspace"]["id"].as_u64().unwrap(),
        mon.1,
        (stats["x"].as_u64().unwrap(), stats["y"].as_u64().unwrap()),
        if stats["transform"].as_u64().unwrap() % 2 == 0 {
            res
        } else {
            (res.1, res.0)
        },
        get_scale(stats["scale"].as_f64().unwrap()),
    )
}

fn get_scale(mon_scale: f64) -> f64 {
    if mon_scale.fract() == 0.0 {
        return mon_scale;
    }

    let whole = mon_scale.trunc() as i32;
    let frac = mon_scale.fract().abs();

    let mut best_den = 1;
    let mut s_diff = f64::MAX;

    for den in 1..=MAX_DEN {
        let dec = 1.0 / den as f64;
        let diff = (frac - dec).abs();
        if diff < s_diff {
            s_diff = diff;
            best_den = den;
            if diff < 1e-10 {
                break;
            }
        }
    }
    (whole * best_den as i32 + 1) as f64 / best_den as f64
}

fn parse_arr(inp: String) -> u64 {
    inp.trim_matches('"')
        .split_whitespace()
        .collect::<Vec<&str>>()[0]
        .parse()
        .unwrap()
}

// inner, outer, border
pub fn get_gaps() -> (u64, u64, u64) {
    (
        parse_arr(
            send_cmd("getoption general:gaps_in".to_string(), true).unwrap()["custom"].to_string(),
        ),
        parse_arr(
            send_cmd("getoption general:gaps_out".to_string(), true).unwrap()["custom"].to_string(),
        ),
        send_cmd("getoption general:border_size".to_string(), true).unwrap()["int"]
            .as_u64()
            .unwrap(),
    )
}
