use crate::common;
use crate::workspace;

pub fn get_last_x(saved: Vec<common::Client>, y: u64) -> u64 {
    let raw = get_xs_r(saved, y);
    *raw.first().unwrap_or(&workspace::DEF_X)
}

pub fn get_ys(saved: Vec<common::Client>, cur_x: u64, cur_y: u64) -> Vec<u64> {
    let mut raw: Vec<u64> = saved.iter().map(|c| c.ws.1).collect();
    raw.sort_unstable();
    raw.dedup();

    if !saved.iter().any(|c| c.ws == (cur_x, cur_y)) {
        match raw.binary_search(&cur_y) {
            Ok(index) => {
                raw.remove(index);
            }
            Err(_) => {}
        }
    }
    raw
}

pub fn get_xs(saved: Vec<common::Client>, cur_y: u64) -> Vec<u64> {
    let mut raw = get_xs_r(saved, cur_y);
    raw.sort_unstable();
    raw.dedup();
    raw
}

fn get_xs_r(saved: Vec<common::Client>, y: u64) -> Vec<u64> {
    saved
        .into_iter()
        .filter_map(|c| if c.ws.1 == y { Some(c.ws.0) } else { None })
        .collect()
}
