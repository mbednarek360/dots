pub fn get_id(x: u64, y: u64, mon: u64) -> u64 {
    x | (y << 8) | (mon << 16)
}

pub fn parse_id(id: u64) -> (u64, u64) {
    ((id & 0xff), ((id >> 8) & 0xff))
}
