use crate::common;

use ratatui::{
    layout::Rect,
    style::Color,
    widgets::canvas::{Line, Rectangle},
};

const REC_WIDTH: i64 = 17;
const REC_HEIGHT: i64 = 7;
const V_SPACING: i64 = 0;
const H_SPACING: i64 = 2;

pub const C_WIDTH: f64 = 10.0;
pub const C_HEIGHT: f64 = 10.0;

pub fn eq(a: f64, b: f64) -> bool {
    (a - b).abs() < 1e-10
}

pub fn client_to_box(
    color: Color,
    gaps: (u64, u64, u64),
    mon_pos: (u64, u64),
    mon_size: (u64, u64),
    mon_scale: f64,
    client: &common::Client,
) -> Rectangle {
    let gaps_in = gaps.0;
    let gaps_out = gaps.1;
    let border = gaps.2;

    let m_width = ((mon_size.0 as f64 / mon_scale) as u64 - gaps_out) as f64;
    let m_height = ((mon_size.1 as f64 / mon_scale) as u64 - gaps_out) as f64;

    let x_pos = client.pos.0 as f64 - mon_pos.0 as f64 - (gaps_out + border) as f64;
    let y_pos = client.pos.1 as f64 - mon_pos.1 as f64 - (gaps_out + border) as f64;

    let x_size = client.size.0 as f64 + ((gaps_in + border) * 2) as f64;
    let y_size = client.size.1 as f64 + ((gaps_in + border) * 2) as f64;

    let width = (x_size / m_width) * C_WIDTH;
    let height = (y_size / m_height) * C_HEIGHT;
    let x = (x_pos / m_width) * C_WIDTH;
    let y = C_HEIGHT - height - ((y_pos / m_height) * C_HEIGHT);

    Rectangle {
        width,
        height,
        x,
        y,
        color,
    }
}

pub fn make_area(area: Rect, rel: (i64, i64)) -> Rect {
    let rel_x = (rel.0 * (REC_WIDTH + H_SPACING)) - (REC_WIDTH / 2);
    let rel_y = (rel.1 * (REC_HEIGHT + V_SPACING)) - (REC_HEIGHT / 2);

    Rect {
        x: ((area.width / 2) as i64 + rel_x) as u16,
        y: ((area.height / 2) as i64 + rel_y) as u16,
        width: REC_WIDTH as u16,
        height: REC_HEIGHT as u16,
    }
    .intersection(area)
}

pub fn cull_edge(line: &Line) -> Line {
    Line {
        x1: line.x1.max(0.0).min(C_WIDTH),
        x2: line.x2.max(0.0).min(C_WIDTH),
        y1: line.y1.max(0.0).min(C_WIDTH),
        y2: line.y2.max(0.0).min(C_WIDTH),
        color: line.color,
    }
}

pub fn del_line(line: &Line) -> bool {
    (eq(line.x1, 0.0) && eq(line.x2, 0.0))
        || (eq(line.x1, C_WIDTH) && eq(line.x2, C_WIDTH))
        || (eq(line.y1, 0.0) && eq(line.y2, 0.0))
        || (eq(line.y1, C_HEIGHT) && eq(line.y2, C_HEIGHT))
}
