use crate::common;
use crate::overview::canvas;
use ratatui::{
    style::Color,
    widgets::canvas::{Line, Rectangle},
};

#[derive(Clone)]
pub struct Label {
    pub pos: (f64, f64),
    pub text: String,
    pub color: Color,
    pub bold: bool,
}

fn make_label(
    client: common::Client,
    rect: &Rectangle,
    cur_pos: (i64, i64),
    is_cur: bool,
) -> Label {
    let l_x = rect.x + (rect.width / 2.0);
    let l_y = rect.y + (rect.height / 2.0);
    let bl = client.pos == cur_pos && is_cur;
    let a_color = if bl { Color::White } else { rect.color };
    let name = client
        .class
        .chars()
        .next()
        .unwrap_or(' ')
        .to_uppercase()
        .to_string();
    Label {
        pos: (l_x, l_y),
        text: name,
        color: a_color,
        bold: bl,
    }
}

pub fn get_lines(
    cur_pos: (i64, i64),
    is_cur: bool,
    rects: Vec<Rectangle>,
    clients: Vec<common::Client>,
) -> (Vec<Line>, Vec<Label>) {
    let mut line_vec: Vec<Line> = vec![];
    let mut label_vec: Vec<Label> = vec![];

    for (rect, client) in rects.into_iter().zip(clients) {
        line_vec = line_vec
            .iter()
            .flat_map(|line| split_line(&rect, line))
            .collect();
        label_vec = label_vec
            .into_iter()
            .map(|label| {
                if label.pos.0 > rect.x
                    && label.pos.1 > rect.y
                    && label.pos.0 < (rect.x + rect.width)
                    && label.pos.1 < (rect.y + rect.height)
                {
                    Label {
                        pos: label.pos,
                        text: label.text,
                        bold: false,
                        color: Color::DarkGray,
                    }
                } else {
                    label
                }
            })
            .collect();

        line_vec.append(&mut rect_to_lines(&rect));
        label_vec.push(make_label(client, &rect, cur_pos, is_cur))
    }

    (line_vec, label_vec)
}

fn split_line(rect: &Rectangle, line: &Line) -> Vec<Line> {
    let x1 = rect.x;
    let x2 = rect.x + rect.width;
    let y1 = rect.y;
    let y2 = rect.y + rect.height;

    let is_inter = if canvas::eq(line.x1, line.x2) {
        // vertical
        (line.x1 > x1 && line.x1 < x2)
            && ((line.y1 < y1 && line.y2 > y1)
                || (line.y1 < y2 && line.y2 > y2)
                || (line.y1 > y1 && line.y2 < y2))
    } else {
        // horizontal
        (line.y1 > y1 && line.y1 < y2)
            && ((line.x1 < x1 && line.x2 > x1)
                || (line.x1 < x2 && line.x2 > x2)
                || (line.x1 > x1 && line.x2 < x2))
    };

    let mut out = vec![line.clone(), line.clone()];

    if is_inter {
        if canvas::eq(line.y1, line.y2) {
            out[0].x2 = line.x2.min(x1).max(line.x1);
            out[1].x1 = line.x1.max(x2).min(line.x2);
        } else {
            out[0].y2 = line.y2.min(y1).max(line.y1);
            out[1].y1 = line.y1.max(y2).min(line.y2);
        }
    } else {
        out.truncate(1);
    }
    out.into_iter()
        .filter(|l| !(canvas::eq(l.x1, l.x2) && canvas::eq(l.y1, l.y2)))
        .collect()
}

fn rect_to_lines(rec: &Rectangle) -> Vec<Line> {
    let p1 = (rec.x, rec.y);
    let p2 = (rec.x + rec.width, rec.y);
    let p3 = (rec.x, rec.y + rec.height);
    let p4 = (rec.x + rec.width, rec.y + rec.height);
    let out = vec![
        Line {
            x1: p1.0,
            y1: p1.1,
            x2: p2.0,
            y2: p2.1,
            color: rec.color,
        },
        Line {
            x1: p1.0,
            y1: p1.1,
            x2: p3.0,
            y2: p3.1,
            color: rec.color,
        },
        Line {
            x1: p2.0,
            y1: p2.1,
            x2: p4.0,
            y2: p4.1,
            color: rec.color,
        },
        Line {
            x1: p3.0,
            y1: p3.1,
            x2: p4.0,
            y2: p4.1,
            color: rec.color,
        },
    ];
    out.iter()
        .map(|l| canvas::cull_edge(l))
        .filter(|l| !canvas::del_line(l))
        .collect()
}
