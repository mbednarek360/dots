use crate::common;

// returns list of workspace relative coords and associated clients
pub fn get_rel_coords(
    saved_r: Vec<common::Client>,
    cur_x: u64,
    cur_y: u64,
) -> Vec<((i64, i64), Vec<common::Client>)> {
    let mut saved = saved_r.clone();
    let empty = !saved.iter().any(|c| c.ws == (cur_x, cur_y));
    if empty {
        saved.insert(
            0,
            common::Client {
                ws: (cur_x, cur_y),
                class: "".to_string(),
                pos: (0, 0),
                size: (0, 0),
                float: false,
            },
        );
    }

    let mut saved_z = saved_r.clone();
    saved_z.sort_by_key(|c| !c.float);
    saved_z.reverse();

    let ys = common::get_ys(saved.clone(), cur_x, cur_y);
    let xs: Vec<Vec<u64>> = (0..ys.len())
        .map(|y| common::get_xs(saved.clone(), ys[y as usize]))
        .collect();

    let y_center = ys.binary_search(&cur_y).unwrap() as i64;
    let x_centers = (0..ys.len()).zip(xs.clone()).map(|(y, xs)| {
        let last_x = common::get_last_x(saved.clone(), ys[y as usize]);
        xs.binary_search(if ys[y as usize] == cur_y {
            &cur_x
        } else {
            &last_x
        })
        .unwrap() as i64
    });

    let client_vec = (0..ys.len())
        .flat_map(|y| (0..xs[y].len()).map(move |x| (x, y)))
        .map(|(x, y)| {
            let cur = (xs[y][x], ys[y]);
            saved_z
                .iter()
                .enumerate()
                .filter(move |(_, c)| c.ws == cur)
                .map(|(index, _)| index)
        })
        .map(|nps| nps.map(|n| saved_z[n].clone()).collect());

    let rels = (0..ys.len()).zip(x_centers).flat_map(|(y, x_center)| {
        (0..xs[y].len()).map(move |x| (x as i64 - x_center, y as i64 - y_center))
    });

    rels.zip(client_vec).collect()
}
