use crate::common;
use crate::overview::canvas;
use crate::overview::rel;
use crate::overview::util;
use ratatui::{
    prelude::CrosstermBackend,
    style::{Color, Style, Stylize},
    symbols::Marker,
    widgets::canvas::Canvas,
    widgets::{Block, Borders},
    Terminal,
};
use std::io;
use std::{thread, time};

const POLL_MS: u64 = 10;

pub fn draw_frame(terminal: &mut Terminal<CrosstermBackend<io::Stdout>>, gaps: (u64, u64, u64)) {
    let (cur_id, mon, mon_pos, mon_size, mon_scale) = common::get_current();
    let (cur_x, cur_y) = common::parse_id(cur_id);
    let saved = common::parse_cache(mon);
    let cur_pos = match saved.first() {
        Some(l) => l.pos,
        None => (0, 0),
    };

    let rels = rel::get_rel_coords(saved, cur_x, cur_y);

    terminal.draw(|frame| {
        let recs: Vec<_> = rels
            .iter()
            .map(|rel| {
                let is_cur = rel.0 == (0, 0);
                let color = if is_cur { Color::Green } else { Color::Blue };

                let rects = rel.1.iter().map(|client| {
                    canvas::client_to_box(color, gaps, mon_pos, mon_size, mon_scale, &client)
                });

                let names = rel.1.clone();

                let (lines, labels) = util::get_lines(cur_pos, is_cur, rects.collect(), names);

                (color, canvas::make_area(frame.area(), rel.0), lines, labels)
            })
            .collect();

        for rec in recs {
            frame.render_widget(
                Canvas::default()
                    .block(
                        Block::default()
                            .borders(Borders::ALL)
                            .style(Style::new().fg(rec.0)),
                    )
                    .marker(Marker::Braille)
                    .x_bounds([0.0, canvas::C_WIDTH])
                    .y_bounds([0.0, canvas::C_HEIGHT])
                    .paint(|ctx| {
                        rec.2.iter().for_each(|line| {
                            ctx.draw(line);
                        });
                        rec.3.iter().for_each(|name| {
                            if name.text != " ".to_string() {
                                let text = name.clone().text.fg(name.color);
                                ctx.print(
                                    name.pos.0,
                                    name.pos.1,
                                    if name.bold { text.bold() } else { text },
                                )
                            }
                        });
                    }),
                rec.1,
            )
        }
    }).unwrap();
}

pub fn overview() -> io::Result<()> {
    let mut terminal = ratatui::init();
    terminal.clear()?;
    let gaps = common::get_gaps();
    loop {
        draw_frame(&mut terminal, gaps);
        thread::sleep(time::Duration::from_millis(POLL_MS));
    }
}
