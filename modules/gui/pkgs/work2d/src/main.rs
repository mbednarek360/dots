mod overview;
mod workspace;
mod common;

use std::{env, io};

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.contains(&"overview".to_string()) {
        overview::overview()
    } 
    else if args.contains(&"init".to_string()) {
        workspace::init();
        Ok(())
    } 
    else {
        let next = args.contains(&"next".to_string());
        let mv = args.contains(&"move".to_string());
        let silent = args.contains(&"silent".to_string());
        let vert = args.contains(&"vert".to_string());
        workspace::change(next, mv, silent, vert);
        Ok(())
    }
}
