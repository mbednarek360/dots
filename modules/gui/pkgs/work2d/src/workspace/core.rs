use crate::common;

// xs guaranteed to be sorted
// will never return a value not in xs if cur not in xs
fn next_w(xs: Vec<u64>, cur: u64, next: bool) -> u64 {
    if xs.len() == 0 {
        cur
    } else {
        match xs.binary_search(&cur) {
            Ok(index) => {
                if next {
                    if cur + 1 > xs[xs.len() - 1] {
                        cur + 1
                    } else {
                        xs[index + 1]
                    }
                } else {
                    if cur - 1 < xs[0] {
                        cur - 1
                    } else {
                        xs[index - 1]
                    }
                }
            }
            Err(index) => {
                if cur > xs[xs.len() - 1] {
                    if next {
                        cur
                    } else {
                        xs[xs.len() - 1]
                    }
                } else if cur < xs[0] {
                    if next {
                        xs[0]
                    } else {
                        cur
                    }
                } else {
                    let d = if next { 0 } else { 1 };
                    xs[index - d]
                }
            }
        }
    }
}

pub fn change(next: bool, mv: bool, silent: bool, vert: bool) {
    let (cur_id, mon, _, _, _) = common::get_current();
    let (cur_x, cur_y) = common::parse_id(cur_id);
    let saved = common::parse_cache(mon);

    let (mut x, mut y) = (0u64, 0u64);
    if vert {
        y = next_w(common::get_ys(saved.clone(), cur_x, cur_y), cur_y, next);
        if y == cur_y {
            x = cur_x
        } else {
            x = common::get_last_x(saved, y);
        }
    } else {
        x = next_w(common::get_xs(saved, cur_y), cur_x, next);
        y = cur_y;
    }

    common::change_anim(vert);
    common::set_ws(common::get_id(x, y, mon), mv, silent);
}
