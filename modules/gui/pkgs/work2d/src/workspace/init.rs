use crate::common;

pub const DEF_Y: u64 = 128;
pub const DEF_X: u64 = 128;

pub fn init() {
    let mut ids = common::get_mon_ids();
    ids.reverse();
    for id in ids {
        common::switch_mon(id);
        common::set_ws(common::get_id(DEF_X, DEF_Y, id), false, false);
    }
}
