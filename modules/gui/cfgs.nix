{ pkgs, ... }:

{
  system.activationScripts.dots_gui.text = ''
    #!/bin/sh
    mkdir -p /home/mrb/.config
    cp -rf ${../../modules/gui/configs}/* /home/mrb/.config
    chown -R mrb /home/mrb/.config
    chmod -R 0777 /home/mrb/.config
  '';

  system.activationScripts.colors_gui = {
    deps = [ "dots_gui" ];
    text =
      let
        renderTemplates = import ../../lib/renderTemplates.nix;
      in
      renderTemplates pkgs "gui" [
        "kitty/colors.conf"
        "hypr/hyprland.conf"
        "hypr/hyprlock.conf"
        "cava/config"
        "dunst/dunstrc.d/colors.conf"
        "rofi/colors.rasi"
      ];
  };
}
