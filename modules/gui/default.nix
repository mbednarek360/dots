{ pkgs, ... }:

{
  imports = [
    ./pkgs.nix
    ./login.nix
    ./cfgs.nix
    ../misc/school.nix
  ];

  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
  };

  xdg.portal = {
    enable = true;
    extraPortals = with pkgs; [
      xdg-desktop-portal-hyprland
    ];
  };

  services.udev.enable = true;
  services.udev.packages = [
    (pkgs.writeTextFile {
      name = "evolv";
      text = ''
        SUBSYSTEMS=="usb", ATTRS{idVendor}=="268b", MODE="0666"
      '';
      destination = "/etc/udev/rules.d/10-local.rules";
    })
    pkgs.android-udev-rules
  ];

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    wireplumber.enable = true;
  };

  services.pipewire.extraConfig.pipewire."92-hifi" = {
    "context.properties" = {
      "default.clock.rate" = 192000;
    };
  };

  security.rtkit.enable = true;
  services.ratbagd.enable = true;
  services.upower.enable = true;
  hardware.graphics.enable = true;

  environment.sessionVariables.NIXOS_OZONE_WL = "1";
  environment.persistence."/perm".users.mrb.directories = [
    ".cache/mesa_shader_cache"
    ".cache/mesa_shader_cache_db"
  ];
}
