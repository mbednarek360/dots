{ pkgs, ... }:

{
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
      timeout = 0;
    };
    kernelPackages = pkgs.linuxPackages_cachyos;
    # kernelParams = [ "fastboot" ];
    consoleLogLevel = 3;
    initrd.verbose = false;
    initrd.systemd.enable = true;
    plymouth.enable = false;
  };

  services.journald.extraConfig = "SystemMaxUse=1G";
  systemd.services = {
    NetworkManager-wait-online.enable = false;
    # systemd-udev-settle.enable = false;
    syncthing.after = pkgs.lib.mkForce [ "multi-user.target" ];
    syncthing-init.wantedBy = pkgs.lib.mkForce [ "syncthing.service" ];
  };
}
