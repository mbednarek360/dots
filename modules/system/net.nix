{ secrets, ... }:

{
  networking = {
    networkmanager.enable = true;
    firewall.enable = true;
    dhcpcd.enable = false;
  };

  networking.nameservers = [
    "45.90.28.0#${secrets.misc.nextdns}.dns.nextdns.io"
    "2a07:a8c0::#${secrets.misc.nextdns}.dns.nextdns.io"
    "45.90.30.0#${secrets.misc.nextdns}.dns.nextdns.io"
    "2a07:a8c1::#${secrets.misc.nextdns}.dns.nextdns.io"
  ];
  
  services.resolved = {
    enable = true;
    dnsovertls = "true";
    fallbackDns = [ ];
  };
  
  services.timesyncd.enable = false;
  services.chrony.enable = true;

  systemd.targets.network.wantedBy = [ "multi-user.target" ];

  environment.persistence."/perm".directories = [
    "/etc/NetworkManager/system-connections"
  ];
}
