{ ... }:

{
  boot.initrd.luks.devices."cryptroot" = {
    allowDiscards = true;
    keyFileSize = 4096;
    bypassWorkqueues = true;
  };

  fileSystems."/" = {
    device = "none";
    fsType = "tmpfs";
    options = [ "noatime" "nodiratime" "mode=755" ];
  };

  fileSystems."/perm" = {
    neededForBoot = true;
    device = "/dev/disk/by-label/NIX-ROOT";
    fsType = "f2fs";
    options = [ "noatime" "nodiratime" "atgc" "gc_merge" "discard" ];
    noCheck = true;
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/NIX-BOOT";
    fsType = "vfat";
  };

  environment.persistence."/perm" = {
    hideMounts = true;
    directories = [
      "/var/lib/nixos"
      "/var/db/sudo"
      "/nix"
    ];
  };
}
