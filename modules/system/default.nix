{ pkgs, ... }:

{
  imports = [
    ./fs.nix
    ./net.nix
    ./boot.nix
    ./pkgs.nix
    ./sync.nix
    ./user.nix
    ./cfgs.nix
  ];

  nix.package = pkgs.lix;
  nix.settings = {
    allowed-users = [ "@wheel" ];
    auto-optimise-store = true;
    experimental-features = [
      "nix-command"
      "flakes"
    ];
  };
  nixpkgs.config.allowUnfree = true;
  chaotic.nyx.cache.enable = true;

  programs.nh = {
    enable = true;
    clean.enable = true;
    clean.extraArgs = "--keep 10";
  };

  time.timeZone = "America/New_York";
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  security.sudo.execWheelOnly = true;
  users.mutableUsers = false;
  users.users.root.hashedPassword = "!";
  environment.binsh = "${pkgs.dash}/bin/dash";
}
