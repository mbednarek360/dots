#!/usr/bin/env nu

def main [name width? height? hpos? vpos?] {
    let type_exact = (file --mime-type $name | split row " " | get 1)
    let type_fuzzy = ($type_exact | split row "/" | get 0)
    match $type_exact {
        "inode/directory" => {
            let dir = (echo $name | path dirname)
            let base = (echo $name | path basename)
            cd $dir
            eza --color=always --icons=always --tree -L 2 $base
        }
        _ => {
            match $type_fuzzy {
                "image" => {
                    tiv -w $width -h $height $name
                }
                _ => {
                    bat --color=always --line-range=:500 $name
                }
            }
        }
    }
}
