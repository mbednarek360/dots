#!/usr/bin/env python3
import sys
import os

NAME = os.environ['ZELLIJ_SESSION_NAME']
CACHE = f'{os.environ['HOME']}/.cache/cwd'

def read_cache():
    try:
        with open(CACHE, 'r') as file:
            return [line.split(':') for line in file.readlines()]       
    except:
        open(CACHE, 'a').close()
        return []

def get_cache():
    lines = read_cache()

    for line in lines:
        if line[0] == NAME:
            return line[1]
    return "."

def is_in(lines):
    for i in range(len(lines)):
        if lines[i][0] == NAME:
            return i
    return None

def set_cache(dir, rem = False):
    lines = read_cache()

    index = is_in(lines)
    if rem:
        lines.pop(index) 
    else:
        if index == None:
            lines.append([NAME, dir])
        else:
            lines[index] = [NAME, dir]

    with open(CACHE, 'w') as file:
        file.writelines([':'.join(line) + '\n' for line in lines])

if sys.argv[1] == "set":
    set_cache(sys.argv[2])
elif sys.argv[1] == "rm":
    set_cache("", True)
else:
    print(get_cache()) 
