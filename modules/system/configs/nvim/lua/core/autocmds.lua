local autocmd = vim.api.nvim_create_autocmd

-- load after enter
autocmd("User", {
    pattern = "VeryLazy",
    callback = function()
        require('core.binds')

        -- twilight
        autocmd("InsertEnter", {
            pattern = "*",
            command = "if empty(&bt) | exec 'TwilightEnable' | exec 'IBLDisable' | endif"
        })
        autocmd("InsertLeave", {
            pattern = "*",
            command = "exec 'TwilightDisable' | exec 'IBLEnable'"
        })

        -- nicities
        autocmd("ExitPre", {
            pattern = "*",
            command = "silent! wa",
        })
        autocmd("VimResized", {
            pattern = "*",
            command = "tabdo wincmd =",
        })
        autocmd("BufWinLeave", {
            pattern = "*",
            command = "silent! mkview!"
        })
    end
})

-- remember line
autocmd("BufWinEnter", {
    pattern = "*",
    command = "silent! loadview",
})
