local M = {}
local C = {}

function C.comment()
    require('nvim_comment').setup {
        create_mappings = false
    }
end

table.insert(M, {
    'terrortylor/nvim-comment',
    config = C.comment,
    cmd = 'CommentToggle'
})

return M
