local M = {}
local C = {}

function C.avante()
    require('avante').setup {
        provider = "claude",
        auto_suggestions_provider = "claude",
        claude = {
            endpoint = "https://api.anthropic.com",
            model = "claude-3-7-sonnet-20250219",
            temperature = 0,
            max_tokens = 16384,
        },
    }
    require('avante_lib').load()
    require('render-markdown').setup()
end

table.insert(M, {
    "yetone/avante.nvim",
    event = "VeryLazy",
    version = false,
    build = "make",
    config = C.avante,
    dependencies = {
        "stevearc/dressing.nvim",
        "nvim-lua/plenary.nvim",
        "MunifTanjim/nui.nvim",
        "nvim-tree/nvim-web-devicons",
        {
            'MeanderingProgrammer/render-markdown.nvim',
            opts = {
                file_types = { "markdown", "Avante" },
            },
            ft = { "markdown", "Avante" },
        },
    },
})

return M
