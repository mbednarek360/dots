let binds = [
   {
      name: lf
      modifier: CONTROL
      keycode: char_f
      mode: [vi_insert, vi_normal, emacs]
      event: {
        send: executehostcommand,
        cmd: "lf",
      }
    }
  ]
