alias s = sudo
alias nv = nvim
alias se = sudoedit
alias ze = zellij edit
alias bv = brave
alias ct = bat
alias lf = cd (^lf -print-last-dir)
alias dev = nix develop -c $env.SHELL

def "rn" [a: path, b:string] {
    mv $a $"($a | path dirname)/($b)"    
}

def "spawn" [...c: string] {
    sh -c $"($c | str join ' ') &"
}

def "trash" [] {
    rm -r .Trash-*
}

def "clean" [] {
    s -v
    nh clean all --keep 10
}

def "update" [] {
    s -v
    nh os boot -u ~/Dots
    clean
}             

def "rebuild" [cmd: string] {
    s -v
    nh os $cmd ~/Dots
    clean
}

def "shell" [...pkgs: string] {
    let pkglist = ($pkgs | each { |p| $"nixpkgs#($p)" })
    nix shell -I ~/Dots ...$pkglist 
}

def "push" [...m: string] {
    git stage -A
    git commit -m $"($m | str join ' ')"
    git push -u origin main
}

def "diff" [] {
    git diff @{upstream}
}

def "pull" [] {
    git reset --hard
    git pull --recurse-submodules
}

def "ssd" [] {
    s -v
    s cryptsetup luksOpen -l 4096 /dev/disk/by-uuid/232823d5-ae5d-496c-a63c-ed1e4dbcd489 ssd
    if $env.LAST_EXIT_CODE == 0 {
        mkdir $"($env.HOME)/SSD"
        s mount /dev/mapper/ssd $"($env.HOME)/SSD"
   } else { sh -c false }
}

def "ssd rm" [] {
    s -v
    s umount $"($env.HOME)/SSD"
    if $env.LAST_EXIT_CODE == 0 {
        rmdir $"($env.HOME)/SSD"
        s fsck.f2fs /dev/mapper/ssd
        s cryptsetup luksClose ssd
    }
}
