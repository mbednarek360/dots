source /home/mrb/.config/nushell/bases.nu

# fzf theming
$env.FZF_DEFAULT_OPTS = $"
    --pointer=' ' --prompt='  '
    --preview-window=border-none --info=hidden --no-scrollbar --no-separator
    --color=bg+:($env.base00),bg:($env.base00),spinner:($env.base0d),hl:($env.base0e)
    --color=fg:($env.base05),header:($env.base0b),info:($env.base0e),pointer:($env.base0d)
    --color=marker:($env.base06),fg+:($env.base0d),prompt:($env.base0d),hl+:($env.base0e)"                             

$env.LS_COLORS = (dircolors ~/.config/dir_colors | split row "'" | get 1)

# starship
$env.PROMPT_INDICATOR = ""
$env.PROMPT_INDICATOR_VI_INSERT = ""
$env.PROMPT_INDICATOR_VI_NORMAL = ""
starship init nu | save -f ~/.cache/starship.nu
