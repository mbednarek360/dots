{ pkgs, ... }:

{
  system.activationScripts.dots_system.text = ''
    #!/bin/sh
    mkdir -p /home/mrb/.config
    cp -rf ${../../modules/system/configs}/* /home/mrb/.config
    chown -R mrb /home/mrb/.config
    chmod -R 0777 /home/mrb/.config 
  '';

  system.activationScripts.colors_system = {
    deps = [ "dots_system" ];
    text =
      let
        renderTemplates = import ../../lib/renderTemplates.nix;
      in
      renderTemplates pkgs "system" [
        "nvim/lua/colors/bases.lua"
        "nushell/bases.nu"
      ];
  };
}
