{ secrets, ... }:

{
  services.syncthing = {
    enable = true;
    user = "mrb";
    configDir = "/home/mrb/.config/syncthing";
    overrideDevices = true;
    overrideFolders = true;
    settings = {
      devices = { "Phone" = { id = secrets.syncthing.phone; }; };
      folders = {
        "Documents" = {
          path = "/home/mrb/Documents";
          devices = [ "Phone" ];
        };
        "Code" = {
          path = "/home/mrb/Code";
          devices = [ "Phone" ];
        };
        "Camera" = {
          path = "/home/mrb/Camera";
          devices = [ "Phone" ];
        };
      };
    };
  };

  environment.persistence."/perm".users.mrb.directories = [{
    directory = ".config/syncthing";
    mode = "0700";
  }];
}
