{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    nushell
    starship
    zoxide
    macchina
    bottom
    lf
    zellij
    fzf
    fd
    ripgrep
    eza
    tiv
    bat
    file
    rclone
    fish
    tty-clock
    gcc
    luarocks
    mosh

    wireguard-tools
    gnumake
    
    nixd
    nixfmt-rfc-style
    lua-language-server
    (python3.withPackages (python-pkgs: [ python-pkgs.jedi-language-server ]))
  ];

  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [
    libgcc
    libGL
    glib
  ];

  programs = {
    git.enable = true;
    neovim.enable = true;
    neovim.defaultEditor = true;
  };

  environment.persistence."/perm".users.mrb = {
    directories = [
      ".local/share/nvim/"
      ".local/state/nvim/"
      ".local/share/zoxide/"
      ".cache/zellij/"
    ];
    files = [
      ".config/nushell/history.sqlite3"
      ".config/nushell/history.sqlite3-shm"
      ".config/nushell/history.sqlite3-wal"
    ];
  };
}
