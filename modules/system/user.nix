{ secrets, pkgs, ... }:

{
  users.users.mrb = {
    isNormalUser = true;
    shell = pkgs.nushell;
    description = "Michael Bednarek";
    extraGroups = [ "networkmanager" "wheel" "dialout" ];
  };

  programs.git.config = {
    user.name = "Michael Bednarek";
    user.email = secrets.misc.email;
  };

  environment.variables = { ANTHROPIC_API_KEY = secrets.misc.claude; };

  environment.persistence."/perm".users.mrb.directories = [
    "Dots/"
    "Code/"
    "Documents/"
    "Camera/"
    "Perm/"
    ".cargo/"
    {
      directory = ".ssh";
      mode = "0700";
    }
  ];
}
