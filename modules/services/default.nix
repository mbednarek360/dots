{ secrets, lib, ... }:

{
  imports = [ ./sync.nix ./jelly.nix ./searx.nix ];

  system.activationScripts.dots_nginx.text = ''
    #!/bin/sh
    cp -r ${./configs/web} /var/www
    chown -R nginx /var/www
  '';

  services.nginx = {
    enable = true;
    virtualHosts = {
      "bednarek.cloud" = {
        forceSSL = true;
        enableACME = true;
        root = "/var/www";
      };
    };
  };

  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = true;
  };

  programs.mosh = {
      enable = true;
      openFirewall = true;
  };

  security.acme = {
    defaults.email = secrets.misc.email;
    acceptTerms = true;
  };

  services.cloudflare-dyndns = {
    enable = true;
    apiTokenFile = builtins.toFile "cloudflare" secrets.misc.cloudflare;
    domains = [ "bednarek.cloud" ];
  };

  networking.firewall.interfaces."wlp0s20f3".allowedTCPPorts = [ 80 443 ];
  systemd.services.NetworkManager-wait-online.enable = lib.mkForce true;

  environment.persistence."/perm".users.mrb.directories =
    [ "Backups" "Media" ".config/rclone" ".cache/rclone" ];

  environment.persistence."/perm".directories =
    [ "/var/lib/acme" "/run/acme" "/run/nginx" ];
}
