#!/usr/bin/env nu

let paths = [ 'Code' 'Documents' 'Camera' ]
let repos = '/home/mrb/Backups'
let home = '/home/mrb'

def db_sync [dir: string] {
    echo $dir
    (sudo -u mrb rclone sync 
        --progress
        --bind 0.0.0.0
        --checkers 12
        --transfers 12
        --use-mmap
        --fast-list
        --tpslimit 12
        --tpslimit-burst 0
        --order-by "size,mixed,50"
        $'($repos)/($dir)' $'pcloud:($dir)'
    )
}

def eng_up [dir: string] {
    engram update $'($repos)/($dir)' $'($home)/($dir)' 1week 
}

def sync_all [] {
    $paths | each { |p| eng_up $p }
    $paths | each { |p| try { db_sync $p } catch { try { db_sync $p } } }
}

def main [] {
    if ($'($home)/.cache/sync.lock' | path exists) {
        exit
    }

    touch $'($home)/.cache/sync.lock'
    sync_all
    rm $'($home)/.cache/sync.lock'
}
