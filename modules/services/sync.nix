{ secrets, pkgs, engram, ... }:

let
  engram_pkg = engram.packages."x86_64-linux".default;
in
{
  services.nginx.virtualHosts."sync.bednarek.cloud" = {
    forceSSL = true;
    useACMEHost = "bednarek.cloud";
    locations."/" = {
      proxyPass = "http://127.0.0.1:8384";
      proxyWebsockets = true;
      recommendedProxySettings = true;
    };
  };

  systemd.timers."backup" = {
    wantedBy = [ "timers.target" ];
    timerConfig = {
      Unit = "backup.service";
      OnCalendar = "*:0/5:00";
    };
  };

  environment.systemPackages = [
    engram_pkg
  ];

  systemd.services."backup" = {
    path = with pkgs; [
      sudo
      rclone
      engram_pkg
    ];
    script = ''
      ${pkgs.nushell}/bin/nu ${./configs/sync/sync.nu} 
    '';
    serviceConfig = {
      StandardOutput = "null";
      Type = "oneshot";
    };
  };

  security.acme.certs."bednarek.cloud".extraDomainNames = [ "sync.bednarek.cloud" ];
  services.cloudflare-dyndns.domains = [ "sync.bednarek.cloud" ];
}
