{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    jellyfin
    jellyfin-web
    jellyfin-ffmpeg
  ];

  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };

  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver
      vaapiIntel
      vaapiVdpau
      libvdpau-va-gl
      intel-compute-runtime
    ];
  };

  services.jellyfin = {
    enable = true;
    user = "mrb";
  };

  services.nginx.virtualHosts."jelly.bednarek.cloud" = {
    forceSSL = true;
    useACMEHost = "bednarek.cloud";
    locations."/" = {
      proxyPass = "http://127.0.0.1:8096";
      proxyWebsockets = true;
      recommendedProxySettings = true;
    };
  };

  security.acme.certs."bednarek.cloud".extraDomainNames =
    [ "jelly.bednarek.cloud" ];
  services.cloudflare-dyndns.domains = [ "jelly.bednarek.cloud" ];

  environment.persistence."/perm".directories =
    [ "/var/lib/jellyfin" "/var/cache/jellyfin" ];
}
