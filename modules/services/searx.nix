{ ... }:

{
  services.searx = {
    enable = true;
    settings =
      builtins.fromJSON (builtins.readFile ./configs/searx/settings.json);
  };

  services.nginx.virtualHosts."search.bednarek.cloud" = {
    forceSSL = true;
    useACMEHost = "bednarek.cloud";
    locations."/" = {
      proxyPass = "http://127.0.0.1:8888/";
      proxyWebsockets = true;
      recommendedProxySettings = true;
    };
  };

  security.acme.certs."bednarek.cloud".extraDomainNames =
    [ "search.bednarek.cloud" ];
  services.cloudflare-dyndns.domains = [ "search.bednarek.cloud" ];
}
