{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    zotero
  ];

  environment.persistence."/perm".users.mrb.directories = [
    ".zotero/"
  ];
}
