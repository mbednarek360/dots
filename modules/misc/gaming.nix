{ pkgs, ... }:

{
  hardware.graphics.enable32Bit = true;
  chaotic.hdr.enable = true;

  environment.systemPackages = with pkgs; [
    pkgsi686Linux.gperftools
    oversteer
    nss
  ];

  programs = {
    steam = {
      package = pkgs.steam.override {
        extraLibraries = pkgs: [ pkgs.nss ];
      };

      enable = true;
      gamescopeSession.enable = true;
      gamescopeSession.args = [
        "-f"
        "-e"
        "--adaptive-sync"
        "--hdr-enabled"
        "--hdr-debug-force-support"
        "--hdr-debug-force-output"
        "--hdr-sdr-content-nits 400"
        "--hdr-itm-enable"
        "--hdr-itm-target-nits 400"
        "-W 2560"
        "-H 1440"
        "-r 144"
        "-O DP-1"
        "--force-grab-cursor"
      ];
    };
  };

  environment.variables.ENABLE_HDR_WSI = 1;
  environment.variables.DXVK_HDR = 1;
  environment.variables.ENABLE_GAMESCOPE_WSI = 1;

  environment.persistence."/perm".users.mrb.directories = [
    ".local/share/Steam"
    ".local/share/BeamNG.drive"
    ".config/oversteer"
  ];
}
