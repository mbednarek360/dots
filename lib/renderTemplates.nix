pkgs: type: templates:
builtins.foldl' (x: y: x + y)
  ''
    #/bin/sh
    mkdir -p /home/mrb/.cache
    ${pkgs.python3}/bin/python3 ${./base16.py} \
    ${../modules/system/configs/colors.yml} > /home/mrb/.cache/colors.yml
  ''
  (
    builtins.map (template: ''
      ${pkgs.mustache-go}/bin/mustache /home/mrb/.cache/colors.yml \
      ${../modules}/${type}/configs/${template}.mustache > \
      /home/mrb/.config/${template} ; 
    '') templates
  )
