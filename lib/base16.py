#!/usr/bin/env python
import sys

with open(sys.argv[1], 'r') as f:
    data = f.read().split("\n")[0:16+1]

bases = [s.split('"')[1::2][0] for s in data[0:16]]
rgb = [[int(base[i:i+2], 16) for i in [0, 2, 4]] for base in bases]
opacity = float(data[16].split(': ')[-1]) 

out = ""
for i in range(16):
    out += 'base0{:X}: "{}"\n'.format(i, bases[i]) 
for i in range(16):
    out += 'base0{:X}-r: "{}"\n'.format(i, rgb[i][0]) 
    out += 'base0{:X}-g: "{}"\n'.format(i, rgb[i][1]) 
    out += 'base0{:X}-b: "{}"\n'.format(i, rgb[i][2])

out += 'opacity: "{}"\n'.format(opacity)
out += 'opacity-x: "{:02x}"'.format(int(0xff * opacity))

print(out)
