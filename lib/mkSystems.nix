pkgs: chaotic: impermanence: secrets: engram: hosts:
builtins.listToAttrs (
  builtins.map (host: {
    name = "michael-${host}";
    value = pkgs.lib.nixosSystem {
      specialArgs = {
        inherit secrets;
        inherit engram;
      };
      modules = [
        { networking.hostName = "michael-${host}"; }
        impermanence.nixosModule
        chaotic.nixosModules.default
        ../hosts/${host}
        ../modules/system
      ];
    };
  }) hosts
)
