{
  description = "Michael's NixOS Config";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    chaotic.url = "github:chaotic-cx/nyx/nyxpkgs-unstable";
    impermanence.url = "github:nix-community/impermanence";
    secrets.url = "git+ssh://git@gitlab.com/mbednarek360/secrets.git?ref=main";
    engram.url = "gitlab:mbednarek360/engram";
  };

  outputs = { nixpkgs, chaotic, impermanence, secrets, engram, ... }: {
    nixosConfigurations =
      let
        hosts = builtins.attrNames (builtins.readDir ./hosts);
        mkSystems = import ./lib/mkSystems.nix;
      in
      mkSystems nixpkgs chaotic impermanence secrets engram hosts;
  };
}
