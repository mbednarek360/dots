{ secrets, lib, ... }:

{
  imports = [
    ./hardware.nix
    ../../modules/gui
  ];

  users.users.mrb.password = secrets.passwd.laptop;

  services.logind = {
    lidSwitch = "suspend";
    powerKey = "poweroff";
    powerKeyLongPress = "reboot";
  };

  hardware.sensor.iio.enable = lib.mkDefault true;
  services.power-profiles-daemon.enable = true;
  services.fwupd.enable = true;

  services.syncthing.settings.devices."Server" = { id = secrets.syncthing.server; };
  services.syncthing.settings.folders = {
    "Documents".devices = [ "Server" ];
    "Code".devices = [ "Server" ];
    "Camera".devices = [ "Server" ];
  };
}
