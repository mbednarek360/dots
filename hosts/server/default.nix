{ secrets, ... }:

{
  imports = [
    ./hardware.nix
    ../../modules/services
  ];

  users.users.mrb.password = secrets.passwd.server;

  services.logind = {
    lidSwitch = "ignore";
    powerKey = "poweroff";
    powerKeyLongPress = "reboot";
  };

  swapDevices = [ {
    device = "/perm/swapfile";
    size = 16*1024;
  } ];

  services.syncthing.settings.devices."Laptop" = { id = secrets.syncthing.laptop; };
  services.syncthing.settings.devices."Desktop" = { id = secrets.syncthing.desktop; };
  services.syncthing.settings.folders = {
    "Documents".devices = [ "Laptop" "Desktop" ];
    "Code".devices = [ "Laptop" "Desktop" ];
    "Camera".devices = [ "Laptop" "Desktop" ];
  };
  services.syncthing.guiAddress = "0.0.0.0:8384";
}
