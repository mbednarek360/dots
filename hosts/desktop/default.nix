{ secrets, ... }:

{
  imports = [
    ./hardware.nix
    ../../modules/gui
    ../../modules/misc/gaming.nix
  ];
  
  users.users.mrb.password = secrets.passwd.desktop;

  services.logind = {
    powerKey = "poweroff";
    powerKeyLongPress = "reboot";
  };

  services.syncthing.settings.devices."Server" = { id = secrets.syncthing.server; };
  services.syncthing.settings.folders = {
    "Documents".devices = [ "Server" ];
    "Code".devices = [ "Server" ];
    "Camera".devices = [ "Server" ];
  };
}
