{ pkgs, lib, modulesPath, ... }:

{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "nvme" "usb_storage" "sd_mod" "usbhid" "aesni_intel" "cryptd" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelParams = [ 
    "amd_pstate=active"
    "processor.max_cstate=5" 
    "rcu_nocbs=0-15"
    "amdgpu.runpm=0"
  ];

  boot.initrd.luks.devices."cryptroot" = {
    device = "/dev/nvme0n1p2";
    keyFile = "/dev/disk/by-id/usb-Walgreen_Infinitive_03026830041021044031-0:0";
  };
  swapDevices = [ ];

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";
  hardware.cpu.amd.updateMicrocode = true;
  
  hardware.new-lg4ff.enable = true;
  hardware.keyboard.qmk.enable = true;
  services.udev.packages = [
    pkgs.oversteer
    (pkgs.writeTextFile { name = "amdgpu";
      text = ''
        KERNEL=="card0", SUBSYSTEM=="drm", DRIVERS=="amdgpu", ATTR{device/power_dpm_force_performance_level}="manual", ATTR{device/pp_power_profile_mode}="1"
      '';
      destination = "/etc/udev/rules.d/30-amdgpu-pm.rules";
    })             
  ];             
}
